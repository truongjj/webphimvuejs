import Vue from 'vue'
import Router from 'vue-router'
import User from '@/components/User'
import Login from '@/components/Login'
import Register from '@/components/Register'
import UploadAvatar from '@/components/UploadAvatar'
import Home from '@/components/Home/Home.vue'
import Admin from '@/components/Admin/Admin.vue'
import Film from '@/components/Admin/Film.vue'
import AddNewFilm from '@/components/Admin/AddNewFilm.vue'
import UpdateFilm from '@/components/Admin/UpdateFilm.vue'
import Category from '@/components/Admin/Category.vue'
import TypeMovie from '@/components/Admin/TypeMovie.vue'
import Nation from '@/components/Admin/Nation.vue'
import Actor from '@/components/Admin/Actor.vue'
import Episode from '@/components/Admin/Episode.vue'
import FilmDetail from '@/components/Film/FilmDetail.vue'
import Page404 from '@/components/Page404.vue'
import Archive from '@/components/Archive.vue'
import Search from '@/components/Search.vue'
Vue.use(Router)

export default new Router({
  // mode:'history',//remove #
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin,
      children:[
        {
          path:'film',
          name:"Film",
          component:Film,
        },
        {
          path:'add-new-film',
          name:"AddNewFilm",
          component:AddNewFilm
        },
        {
          path:'update-film',
          name:"UpdateFilm",
          component:UpdateFilm
        },
        {
          path:'episode',
          name:"Episode",
          component:Episode
        },
        {
          path:'category',
          name:"Category",
          component:Category
        },
        {
          path:'type-movie',
          name:"TypeMovie",
          component:TypeMovie
        },
        {
          path:'nation',
          name:"Nation",
          component:Nation
        },
        {
          path:'actor',
          name:"Actor",
          component:Actor
        },
       
      ]
    },
    {
      path: '/watch/:filmName',
      name: 'FilmDetail',
      component: FilmDetail,
    },
    {
      path: '/archive',
      name: 'Archive',
      component: Archive,
    },
    {
      path: '/register',
      name: 'Register',
      component: Register,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/user',
      name: 'User',
      component: User,
      children:[
        {
          path:'upload-avatar',
          name:"UploadAvatar",
          component:UploadAvatar
        },
      ]
    },
    {
      path: '/search',
      name: 'Search',
      component: Search,
    },
    {
      path: '/404',
      name: 'Page404',
      component: Page404,
    }


  ]
})
