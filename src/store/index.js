
import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user.js'
import film from './modules/film.js'
Vue.use(Vuex)

const store = new Vuex.Store({
  modules:{
 
    user,
    film

  },
  state: { //state
  },
  getters:{//getters
  },
  mutations: {// mutation
  },
  actions:{ // actions
  }
})
export default store