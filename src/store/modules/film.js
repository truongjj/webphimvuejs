import axios from 'axios'
const state = {
    film:[],
    type_movie:[],
    nation:[],
    category:[],
    actor:[],
    resFilmErrors:"",
    resFilmMessages:"",
    filmDetail:{},
    filmCategory:[],
    listFilmCategory:[],
    // listfilmActor:[],
    filmActor:[],
    actorById:{},
    listEpisode:[],
    filmDetailBySlug:{},
    typeMovieById:{},
    nationById:{},
    categoryById:{},
    actorById:{},
    listFilmByCategory:[],
    listFilmByActor:[]
}
const getters = {
    film:state=>state.film.reverse(),
    filmDetail:state=>state.filmDetail,
    type_movie:state=>state.type_movie.reverse(),
    nation:state=>state.nation.reverse(),
    category:state=>state.category.reverse(),
    actor:state=>state.actor.reverse(),
    resFilmErrors:state=>state.resFilmErrors,
    resFilmMessages:state=>state.resFilmMessages,
    filmCategory:state=>state.filmCategory,
    filmActor:state=>state.filmActor,
    listEpisode:state=>state.listEpisode,
    filmDetailBySlug:state=>state.filmDetailBySlug,
    typeMovieById:state=>state.typeMovieById,
    nationById:state=>state.nationById,
    categoryById:state=>state.categoryById,
    actorById:state=>state.actorById,
    listFilmCategory:state=>state.listFilmCategory,
    listFilmByCategory:state=>state.listFilmByCategory,
    listFilmByActor:state=>state.listFilmByActor
}
const mutations = {
    RESET_ALL_STATE(state){
        console.log("adkjhajk")
        state.film=[]
        state.type_movie=[]
        state.nation=[]
        state.category=[]
        state.actor=[]
        state.resFilmErrors="",
        state.resFilmMessages=""
        state.filmDetail={}
        state.filmCategory=[]
        state.listFilmCategory=[]
        //state.listfilmActor=[]
        state.filmActor=[]
        state.actorById={}
        state.listEpisode=[]
        state.filmDetailBySlug={}
        state.typeMovieById={}
        state.nationById={}
        state.categoryById={}
        state.actorById={}
        state.listFilmByCategory=[]
    },
    RESET_FILM_MSG(state){
        state.resFilmErrors=""
        state.resFilmMessages=""
    },
    //Delete
    DELETE_FILM(state,filmId){
        state.film=state.film.filter(film=>film.id!==filmId).reverse()
    },
    DELETE_CATEGORY(state,categoryId){
        state.category=state.category.filter(category=>category.id!==categoryId).reverse()
    },
    DELETE_TYPE_MOVIE(state,typeMovieId){
        state.type_movie=state.type_movie.filter(type_movie=>type_movie.id!=typeMovieId).reverse()
    },
    DELETE_NATION(state,nationId){
        state.nation=state.nation.filter(nation=>nation.id!=nationId).reverse()
    },
    DELETE_ACTOR(state,actorId){
        state.actor=state.actor.filter(actor=>actor.id!=actorId).reverse()
    },
    DELETE_EPISODE(state,episodeId){
        state.listEpisode=state.listEpisode.filter(listEpisode=>listEpisode.id!=episodeId).reverse()
    },
    //Update
    UPDATE_CATEGORY(state,category){
        for(let item of state.category){
            if(item.id===category.id){
                item.name=category.name
            }
        }
    },
    UPDATE_TYPE_MOVIE(state,typemovie){
        for(let item of state.type_movie){
            if(item.id===typemovie.id){
                item.name=typemovie.name
                item.handle=typemovie.handle
            }
        }
    },
    UPDATE_NATION(state,nation){
        for(let item of state.nation){
            if(item.id===nation.id){
                item.name=nation.name
            }
        }
    },
    UPDATE_ACTOR(state,actor){
        for(let item of state.actor){
            if(item.id===actor.id){
                item.name=actor.name,
                item.avatar=actor.avatar
            }
           
        }
    },
    UPDATE_EPISODE(state,episode){
        for(let item of state.listEpisode){
            if(item.id===episode.id){
                item.episode_name=episode.episode_name,
                item.content=episode.content
            }
           
        }
    }
}
const actions = {
    //get all
    async getFilm({ commit }) {
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/film`);
            state.film=response.data
            return response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async getTypeMovie({ commit }) {
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/typemovie`);
            state.type_movie=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }

    },
    async getNation({ commit }) {
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/nation`);
            state.nation=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async getCategory({ commit }) {
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/category`);
            state.category=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
            
        }
    },
    async getActor({ commit }) {
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/actor`);
            state.actor=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
            
        }
    },
    async getFilmCategory({ commit }) {
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/filmcategory`);
            state.listFilmCategory=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
            
        }
    },
    async getFilmByCategoryId({commit},categoryId){
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/filmbycategoryid/${categoryId}`);
            state.listFilmByCategory=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
            
        }
    },
    async getFilmByActorId({commit},actorId){
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/filmbyactorid/${actorId}`);
            state.listFilmByActor=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
            
        }
    },
    //get one
    async getCategoryById({commit},id){
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/category/${id}`);
            state.categoryById=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async getActorById({commit},id){
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/actor/${id}`);
            state.actorById=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async getTypeMovieById({commit},id){
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/typemovie/${id}`);
            state.typeMovieById=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async getNationById({commit},id){
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/nation/${id}`);
            state.nationById=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async getFilmBySlug({commit},slug){
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/film-slug/${slug}`);
            state.filmDetailBySlug=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async getFilmById({ commit },filmId) {
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/film/${filmId}`);
            state.filmDetail=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async getActorById({ commit },id) {
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/actor/${id}`);
            state.actorById=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
            
        }
    },
    async getFilmCategoryByFilmId({commit},filmId){
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/filmcategory/${filmId}`);
            state.filmCategory=response.data
        } catch (error) {
            state.resFilmErrors = "Không lấy được thông tin" // get errors từ server
            state.resFilmMessages = ""
            
        }
    },
    async getFilmActorByFilmId({commit},filmId){
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/filmactor/${filmId}`);
            state.filmActor=response.data
        } catch (error) {
            state.resFilmErrors = "Không lấy được thông tin" // get errors từ server
            state.resFilmMessages = ""
            
        }
    },
    async getEpisodeByFimlID({ commit },film_id) {
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/film-episode/${film_id}`);
            state.listEpisode=response.data
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    }, 

    //update
    async updateFilm({commit},film){
        try {
            const response = await axios.put(process.env.SERVER_HOST+`/film/${film.filmId}`, film.formData,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors = ""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async updateCategory({commit},category){
        try {
            let data={name:category.category_name}
            // console.log(data)
            const response = await axios.put(process.env.SERVER_HOST+`/category/${category.category_id}`, data,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            commit("UPDATE_CATEGORY",{
                id:category.category_id,
                name:category.category_name
            })
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors = ""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async updateTypeMovie({commit},typemovie){
        try {
            let data={
                name:typemovie.typeMovieName,
                handle:typemovie.typeMovieHandle
            }
            const response = await axios.put(process.env.SERVER_HOST+`/typemovie/${typemovie.type_movie_id}`, data,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            commit("UPDATE_TYPE_MOVIE",{
                id:typemovie.type_movie_id,
                name:typemovie.typeMovieName,
                handle:typemovie.typeMovieHandle
            })
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors = ""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async updateNation({commit},nation){
        try {
            let data={
                name:nation.nation_name,
            }
            const response = await axios.put(process.env.SERVER_HOST+`/nation/${nation.nation_id}`, data,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            commit("UPDATE_NATION",{
                id:nation.nation_id,
                name:nation.nation_name,
            })
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors = ""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async updateActor({commit},actor){
        try {
            let formData=new FormData()
            formData=actor.actor_data
            const response=await axios.put(process.env.SERVER_HOST+`/actor/${actor.actor_id}`, actor.actor_data,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            const response2=await axios.get(process.env.SERVER_HOST+`/actor/${actor.actor_id}`)
            commit("UPDATE_ACTOR",response2.data)
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors = ""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async updateEpisode({commit},episode){
        try {
            let data={
                film_id:episode.film_id,
                episode_name:episode.episode_name,
                content:episode.content
            }
            const response = await axios.put(process.env.SERVER_HOST+`/episode/${episode.id}`, data,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            commit("UPDATE_EPISODE",episode)
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors = ""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },

    //delete
    async deleteFilm({ commit },filmId) {
        try {
            const response = await axios.delete(process.env.SERVER_HOST+`/film/${filmId}`,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            commit("DELETE_FILM",filmId)
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors=""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async deleteCategory({commit},category_id){
        try {
            const response = await axios.delete(process.env.SERVER_HOST+`/category/${category_id}`);
            commit("DELETE_CATEGORY",category_id)
            state.resFilmErrors = "" // get errors từ server
            state.resFilmMessages = response.data.message
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
            
        }
    },
    async deleteTypeMovie({ commit },typeMovieId) {
        try {
            const response = await axios.delete(process.env.SERVER_HOST+`/typemovie/${typeMovieId}`,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            commit("DELETE_TYPE_MOVIE",typeMovieId)
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors=""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async deleteNation({ commit }, nationId) {
        try {
            const response = await axios.delete(process.env.SERVER_HOST+`/nation/${nationId}`,{
                headers: {
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            commit("DELETE_NATION",nationId)
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors=""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async deleteActor({ commit }, actorId) {
        try {
            const response = await axios.delete(process.env.SERVER_HOST+`/actor/${actorId}`,{
                headers: {
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            commit("DELETE_ACTOR",actorId)
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors=""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async deleteEpisode({ commit }, episodeId) {
        try {
            const response = await axios.delete(process.env.SERVER_HOST+`/episode/${episodeId}`,{
                headers: {
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            commit("DELETE_EPISODE",episodeId)
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors=""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
   // add
    async addNewFilm({ commit }, newFilm) {
        try {
            const response = await axios.post(process.env.SERVER_HOST+`/film`, newFilm,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors = ""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async addNewCategory({ commit }, newCategory) {
        try {
            const response = await axios.post(process.env.SERVER_HOST+`/category`, newCategory,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            // commit("ADD_CATEGORY",newCategory)
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors = ""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },    
    async addNewTypeMovie({ commit }, newTypeMovie) {
        try {
            const response = await axios.post(process.env.SERVER_HOST+`/typemovie`, newTypeMovie,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors = ""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async addNewNation({ commit }, newNation) {
        try {
            const response = await axios.post(process.env.SERVER_HOST+`/nation`, newNation,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors = ""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async addNewActor({ commit }, newActor) {
        try {
            const response = await axios.post(process.env.SERVER_HOST+`/actor`, newActor,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors = ""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },
    async addEpisode({ commit }, newEpisode) {
        try {
            const response = await axios.post(process.env.SERVER_HOST+`/episode`,newEpisode,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            state.resFilmMessages = response.data.message// get message từ server
            state.resFilmErrors = ""
        } catch (error) {
            state.resFilmErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resFilmMessages = ""
        }
    },

}
export default {
    state,
    getters,
    mutations,
    actions,
}