import axios from 'axios'
const state = {
    resErrors: "",
    resMessages: "",
    isRegistered: false,
    accessToken:"",
    isLoginGlobal:false,
    expJWT:0
}
const getters = {
    resErrors: state => state.resErrors,
    resMessages: state => state.resMessages,
    isRegistered: state => state.isRegistered,
    accessToken:state=>state.accessToken,
    userId:state=>state.userId,
    isLoginGlobal:state=>state.isLoginGlobal,
    expJWT:state=>state.expJWT
}
const mutations = {
    CHANGE_IS_LOGIN_GLOBAL(state,newVal){
        state.isLoginGlobal=newVal
    },
    RESET_MSG(state){
        state.resErrors=""
        state.resMessages=""
    }
}
const actions = {
    async checkExpiredToken({commit}){// Kiểm tra xem token hết hạn hay chưa
        try {// Kiểm tra xem token đã đăng nhập còn hạn hay không
            const response = await axios.get(process.env.SERVER_HOST+`/check`, {
              headers: {
                Authorization: "Bearer " + localStorage.getItem("accessToken"),
              },
            });
            if(response.status===200){
                state.expJWT = response.data.data.exp//get exp jwt
                commit("CHANGE_IS_LOGIN_GLOBAL",true);// thay đổi isLoginGlobal
            }
          } catch (error) {
            commit("CHANGE_IS_LOGIN_GLOBAL",false);// thay đổi isLoginGlobal
           // throw error
          }
    },
    async getUserById({ commit }, id){
        try {
            const response = await axios.get(process.env.SERVER_HOST+`/users/${id}`,{
                headers: { 
                    Authorization:'Bearer '+localStorage.getItem('accessToken')
                }
            });
            let user=response.data;
            return user
        } catch (error) {
            console.log(error)
        }
    },
    async register({ commit }, newUser) {
        try {
            const response = await axios.post(process.env.SERVER_HOST+`/users`, newUser);
            state.resMessages = response.data.message// get message từ server
            state.resErrors = ""
            if (state.resMessages) {// Nếu đăng ký thành công
                state.isRegistered = true
            }
        } catch (error) {
            state.resErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resMessages = ""
        }
    },
    async login({ commit }, user) {
        try {
            const response = await axios.post(process.env.SERVER_HOST+`/login`, user);
            state.resMessages = response.data.message// get message từ server
            state.resErrors = ""
            state.accessToken=response.data.accessToken
            localStorage.setItem("accessToken",response.data.accessToken)//lưu accessToken
            localStorage.setItem("userId",response.data.userId)
            state.isLoginGlobal=true
           
        } catch (error) {
            state.resErrors = error.response.data.error || error.response.data.errors[0].msg // get errors từ server
            state.resMessages = ""
        }
    },
    async logout({ commit }) {
        try {
            let accessToken=localStorage.getItem("accessToken") 
            state.isLoginGlobal=false
            localStorage.removeItem("userId")
            localStorage.removeItem("accessToken")
            await axios.post(process.env.SERVER_HOST+`/logout`,null,{
                headers: { 
                    Authorization:'Bearer '+accessToken
                    }
            });
        } catch (error) {
            
        }
       
        // console.log(state.isLoginGlobal)
    },
    logoutClient() {// Xóa dữ liệu phía client
        state.isLoginGlobal=false
        localStorage.removeItem("userId")
        localStorage.removeItem("accessToken")
    },
    async UploadAvatar({commit},file_userId){
        console.log("upload")
        try {
            let formdata= new FormData()
            formdata.append('file',file_userId.file,file_userId.file.name)
            // console.log(formdata.get("file"))
            let response=await axios.post(process.env.SERVER_HOST+"/avatar/"+file_userId.userId,formdata,{
                headers:{
                    //"Content-Type":'multipart/form-data'
                    Authorization:"Bearer "+localStorage.getItem("accessToken")
                }
            });
            console.log(response.data.message)
          } catch (error) {
            // throw error
            console.log(error.response.data.error)
          }
    },
    
   
}
export default {
    state,
    getters,
    mutations,
    actions,
}