// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import VueDarkMode from "@growthbunker/vuedarkmode"
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Flicking from "@egjs/vue-flicking"
import "@egjs/vue-flicking/dist/flicking.css"
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css'

Vue.use(Vuesax)
Vue.use(Flicking);
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(VueDarkMode, {
  theme: "dark",
  components: [
    "alert", "avatar", "badge", "button", "divider", "heading", "icon",  "progress-bar",  "spinner",
    "checkbox", "file", "image-uploader", "input", "input-numeric", "label", "message", "radios", "select", "tabs", "textarea", "toggle"
  ]
});
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
