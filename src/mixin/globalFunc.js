export default{
    data(){

    },
    async created(){
        
    },
    computed:{
       
    },
    methods:{
        generateUrl(path){
            return process.env.SERVER_HOST+path;
        },
        formatFilmName(str){
            str=str.toString()
            if (str === null || str === undefined) return str;
            str = str.toString().toLowerCase();
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
            str = str.replace(/đ/g, "d");
            str = str.replaceAll(")","");
            str = str.replaceAll("(","");
            str = str.replaceAll(":","");
            str = str.replaceAll("-","");
            return str;
        },
        generateRandomFilm(listFilm,number){
            let list=[];
            let arrays=[];
            for(let i=0;i<number;i++){
                let randomNumber=Math.floor(listFilm.length * Math.random());
                if(arrays.indexOf(randomNumber)===-1){
                    arrays.push(randomNumber);
                }
                else{
                    i--;
                }
            }
            arrays.map((item)=>{
                list.push(listFilm[item]);
            })
            return list;
        }

    }
}